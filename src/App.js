import React, { Component } from 'react';
import { View } from 'react-native';
import firebase from 'firebase';
import { Card, Header, Button, CardSection, Spinner } from './components/common/';
import LoginForm from './components/LoginForm';

class App extends Component {
	constructor() {
		super();
		this.state = { loggedIn: null };
	}

	componentWillMount() {
		// Initialize Firebase
		firebase.initializeApp({
			apiKey: 'AIzaSyBq7WmwTtkE4w4fGe_9V_dg3xgL5G4fYYA',
			authDomain: 'authentication-2ecb4.firebaseapp.com',
			databaseURL: 'https://authentication-2ecb4.firebaseio.com',
			projectId: 'authentication-2ecb4',
			storageBucket: 'authentication-2ecb4.appspot.com',
			messagingSenderId: '312919549507'
		});

		firebase.auth().onAuthStateChanged((user) => {
			if (user) {
				this.setState({ loggedIn: true });
			} else {
				this.setState({ loggedIn: false });
			}
		});
	}

	renderContent() {
		switch (this.state.loggedIn) {
			case true:
				return (
					<CardSection>
						<Button onPress={() => firebase.auth().signOut()}>
							Log Out
						</Button>
					</CardSection >
				);
			case false:
				return <LoginForm />;
			case null:
				return (
					<CardSection>
						<Spinner />
					</CardSection>
				)
		}
	}

	render() {
		return (
			<View>
				<Header headerText="Authentication" />
				{this.renderContent()}
			</View>
		);
	}
}

export default App 